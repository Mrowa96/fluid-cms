# Fluid CMS - DEPRECATED
Simple CMS build with Yii2 and my own libraries.

### Getting started
 * Run ```php init``` in your terminal
 * Set up correct url's (domainLink, frontendLink, backendLink) in ```common/config/bootstrap.php```
 * Import database from ```extras/fc_base.sql.gz```
 * Set up database connection in ```common/config/main.php```
 * That's all, enjoy.
 
### Information
 * Backend login data: Login: admin, Password: q@Werty
 