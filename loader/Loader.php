<?php
    class Loader{
        private $params = null;
        private $root = null;
        private $envs = null;
        private $envNames = null;
        private $envName = null;
        private $url = null;
        private $config = null;

        public function __construct(){
            if (!extension_loaded('openssl')) {
                die('The OpenSSL PHP extension is required by Yii2.');
            }

            $this->root = dirname(__DIR__);

            $this->envs = require("environments/index.php");
            $this->config = require($this->root."/config.php");
            $this->params = $this->getParams();
            $this->envNames = array_keys($this->envs);
            $this->url = 'http://cron.pawel-mrowiec.pl/fc.php';
        }

        public function init(){
            echo "Fluid CMS Application Initialization Tool\n\n";

            //$this->insertKey();
            $this->chooseEnvironment();
            $this->copyEnvironmentData();

            echo "Application was initialized!\n\n";
            exit(0);
        }

        protected function insertKey(){
            if(!isset($this->config['key']) || empty($this->config['key'])){
                echo "Please enter your key to set up the system: ";

                $key = trim(fgets(STDIN));
                $ip = getHostByName(getHostName());
                $options = array(
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HEADER         => false,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_MAXREDIRS      => 10,
                    CURLOPT_ENCODING       => "",
                    CURLOPT_USERAGENT      => "fluid",
                    CURLOPT_AUTOREFERER    => true,
                    CURLOPT_CONNECTTIMEOUT => 120,
                    CURLOPT_TIMEOUT        => 120,
                );

                $ch = curl_init($this->url . "?o=insert-key&key=" . $key . "&ip=" . $ip);
                curl_setopt_array($ch, $options);
                $content  = json_decode(curl_exec($ch));
                curl_close($ch);

                switch($content->status){
                    case 0:
                        echo "This key is already taken. Please contact with author to resolve that problem.\n\n";
                        exit(0);
                    case 1:
                        echo "Key is correct.\n\n";
                        $config = fopen($this->root."/config.php", "w");
                        fwrite($config, "<?php\n return [\n'key' => '".$key."'\n];");
                        fclose($config);
                        break;
                    case 2:
                        echo "You didn't provide the key.\n\n";
                        exit(0);
                    case 3:
                        echo "Key is incorrect.\n\n";
                        exit(0);
                }
            }
            else{
                echo "Key was already configured. Used key: ".$this->config['key'].". Skipping...\n\n";
            }
        }

        protected function chooseEnvironment(){
            if (empty($params['env']) || $params['env'] === '1') {
                echo "Which environment do you want the application to be initialized in?\n\n";

                foreach ($this->envNames as $i => $name) {
                    echo "  [$i] $name\n";
                }

                echo "\n  Your choice [0-" . (count($this->envs) - 1) . ', or "q" to quit] ';
                $answer = trim(fgets(STDIN));

                if (!ctype_digit($answer) || !in_array($answer, range(0, count($this->envs) - 1))) {
                    echo "\n  Quit initialization.\n";
                    exit(0);
                }

                if (isset($this->envNames[$answer])) {
                    $this->envName = $this->envNames[$answer];
                }
            }
            else {
                $this->envName = $this->params['env'];
            }
        }

        protected function copyEnvironmentData(){
            if (!in_array($this->envName, $this->envNames)) {
                $envsList = implode(', ', $this->envNames);
                echo "\n  $this->envName is not a valid environment. Try one of the following: $envsList. \n";
                exit(2);
            }

            $env = $this->envs[$this->envName];

            $files = $this->getFileList($this->root."/loader/environments/{$env['path']}");
            if (isset($env['skipFiles'])) {
                $skipFiles = $env['skipFiles'];
                $root = $this->root;
                array_walk($skipFiles, function(&$value) use($env, $root) { $value = "$root/$value"; });
                $files = array_diff($files, array_intersect_key($env['skipFiles'], array_filter($skipFiles, 'file_exists')));
            }
            $all = false;
            foreach ($files as $file) {
                if (!$this->copyFile($this->root, "/loader/environments/{$env['path']}/$file", $file, $all, $this->params)) {
                    break;
                }
            }

            $callbacks = ['setCookieValidationKey', 'setWritable', 'setExecutable', 'createSymlink'];
            foreach ($callbacks as $callback) {
                if (!empty($env[$callback])) {
                    $this->{$callback}($this->root, $env[$callback]);
                }
            }
        }

        private function getFileList($root, $basePath = ''){
            $files = [];
            $handle = opendir($root);

            while (($path = readdir($handle)) !== false) {
                if ($path === '.git' || $path === '.svn' || $path === '.' || $path === '..') {
                    continue;
                }
                $fullPath = "$root/$path";
                $relativePath = $basePath === '' ? $path : "$basePath/$path";
                if (is_dir($fullPath)) {
                    $files = array_merge($files, $this->getFileList($fullPath, $relativePath));
                } else {
                    $files[] = $relativePath;
                }
            }
            closedir($handle);
            return $files;
        }

        private function copyFile($root, $source, $target, &$all, $params){
            if (!is_file($root . '/' . $source)) {
                echo "       skip $target ($source not exist)\n";
                return true;
            }
            if (is_file($root . '/' . $target)) {
                if (file_get_contents($root . '/' . $source) === file_get_contents($root . '/' . $target)) {
                    echo "  unchanged $target\n";
                    return true;
                }
                if ($all) {
                    echo "  overwrite $target\n";
                } else {
                    echo "      exist $target\n";
                    echo "            ...overwrite? [Yes|No|All|Quit] ";


                    $answer = !empty($params['overwrite']) ? $params['overwrite'] : trim(fgets(STDIN));
                    if (!strncasecmp($answer, 'q', 1)) {
                        return false;
                    } else {
                        if (!strncasecmp($answer, 'y', 1)) {
                            echo "  overwrite $target\n";
                        } else {
                            if (!strncasecmp($answer, 'a', 1)) {
                                echo "  overwrite $target\n";
                                $all = true;
                            } else {
                                echo "       skip $target\n";
                                return true;
                            }
                        }
                    }
                }
                file_put_contents($root . '/' . $target, file_get_contents($root . '/' . $source));
                return true;
            }
            echo "   generate $target\n";
            @mkdir(dirname($root . '/' . $target), 0777, true);
            file_put_contents($root . '/' . $target, file_get_contents($root . '/' . $source));
            return true;
        }

        private function getParams(){
            $rawParams = [];
            if (isset($_SERVER['argv'])) {
                $rawParams = $_SERVER['argv'];
                array_shift($rawParams);
            }

            $params = [];
            foreach ($rawParams as $param) {
                if (preg_match('/^--(\w+)(=(.*))?$/', $param, $matches)) {
                    $name = $matches[1];
                    $params[$name] = isset($matches[3]) ? $matches[3] : true;
                } else {
                    $params[] = $param;
                }
            }
            return $params;
        }

        private function setWritable($root, $paths){
            foreach ($paths as $writable) {
                echo "      chmod 0777 $writable\n";
                @chmod("$root/$writable", 0777);
            }
        }

        private function setExecutable($root, $paths){
            foreach ($paths as $executable) {
                echo "      chmod 0755 $executable\n";
                @chmod("$root/$executable", 0755);
            }
        }

        private function setCookieValidationKey($root, $paths){
            foreach ($paths as $file) {
                echo "   generate cookie validation key in $file\n";
                $file = $root . '/' . $file;
                $fileData = file_get_contents($file);
                $length = 32;
                $bytes = openssl_random_pseudo_bytes($length);
                $key = strtr(substr(base64_encode($bytes), 0, $length), '+/=', '_-.');

                $fileData = str_replace("@cookieValidationKey", $key, $fileData);
                file_put_contents($file, $fileData);
            }
        }

        private function createSymlink($root, $links) {
            foreach ($links as $link => $target) {
                echo "      symlink " . $root . "/" . $target . " " . $root . "/" . $link . "\n";
                //first removing folders to avoid errors if the folder already exists
                @rmdir($root . "/" . $link);
                @symlink($root . "/" . $target, $root . "/" . $link);
            }
        }
    }