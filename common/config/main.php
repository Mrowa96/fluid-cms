<?php
    return [
        'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
        'components' => [
            'db' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'mysql:host=localhost;dbname=fc_base',
                'username' => 'root',
                'password' => 'root',
                'charset' => 'utf8',
                'tablePrefix' => 'fc_'
            ],
            'cache' => [
                'class' => 'yii\caching\FileCache',
            ],
            'authManager' => [
                'class' => 'yii\rbac\DbManager',
            ],
            'request' => [
                'cookieValidationKey' => 'wNS1BryfVizA-sktJa2HVyfYomQlcDvO',
            ],
            'session' => [
                'class' => 'yii\web\DbSession',
                'cookieParams' => [
                    'path' => '/',
                    'domain' => DOMAIN
                ],
            ],
            'mailer' => [
                'class' => 'yii\swiftmailer\Mailer',
                'viewPath' => '@common/mail',
                'useFileTransport' => true,
            ],
        ],
    ];
