<?php
    Yii::setAlias('root', dirname(dirname(__DIR__)));

    Yii::setAlias('common', dirname(__DIR__));
    Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
    Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
    Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
    Yii::setAlias('vendor', dirname(dirname(__DIR__)) . '/vendor');
    Yii::setAlias('loader', dirname(dirname(__DIR__)) . '/loader');
    Yii::setAlias('updates', dirname(dirname(__DIR__)) . '/updates');
    Yii::setAlias('backups', dirname(dirname(__DIR__)) . '/backups');
    Yii::setAlias('data', Yii::getAlias("frontend")."/web/data");

    Yii::setAlias('cdnLink', "http://cdn-s.localhost");
    Yii::setAlias('domainLink', ".dev.localhost");
    Yii::setAlias('frontendLink', "http://dev.localhost");
    Yii::setAlias('backendLink', "http://admin.dev.localhost");

    Yii::setAlias('dataLink', Yii::getAlias("@frontendLink")."/data");

    define("DS", "/");
    define("CDN", Yii::getAlias("@cdnLink"));
    define("DOMAIN", Yii::getAlias("@domainLink"));
