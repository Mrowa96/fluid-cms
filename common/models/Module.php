<?php
    namespace common\models;

    use yii\db\ActiveRecord;

    class Module extends ActiveRecord{

        public function rules(){
            return [
                ['name', 'filter', 'filter' => 'trim'],
                ['name', 'required'],
                ['name', 'string', 'min' => 1, 'max' => 255],
                ['name', 'unique', 'targetClass' => '\common\models\Module', 'message' => 'This name already exists.'],

                ['url', 'filter', 'filter' => 'trim'],
                ['url', 'string', 'max' => 255],

                ['active', 'boolean'],
                [['add_link_to_nav', 'show_default_nav'], 'boolean'],
            ];
        }

        public function attributeLabels(){
            return [
                'name' => 'Nazwa modułu',
                'url' => 'Adres URL',
                'active' => 'Aktywny',
                'add_link_to_nav' => 'Dodaj odnośnik do nawgiacji',
                'show_default_nav' => 'Wyświetl standardową nawigację'
            ];
        }
    }