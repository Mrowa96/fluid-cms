<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;

    class ArticleCategory extends ActiveRecord{

        public function rules(){
            return [
                ['name', 'filter', 'filter' => 'trim'],
                ['name', 'string', 'max' => 255],
                ['name', 'required'],

                ['description', 'filter', 'filter' => 'trim'],
                ['description', 'required'],
            ];
        }

        public function attributeLabels(){
            return [
                'name' => 'Nazwa',
                'description' => 'Opis',
            ];
        }

        public function getForForm()
        {
            $articles = $this->find()->all();
            $result = [];

            if(!empty($articles)){
                foreach($articles AS $article){
                    $result[$article['id']] = $article['name'];
                }
            }

            return $result;
        }
    }