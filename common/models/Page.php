<?php
namespace common\models;

use backend\models\Template;
use yii\db\ActiveRecord;

class Page extends ActiveRecord{

    public $activeLink = false;
    public $staticLink = false;

    public function attributeLabels(){
        return [
            'id' => 'Id',
            'url' => 'Adres Url',
            'keywords' => 'Słowa kluczowe',
            'data' => 'Treść',
            'description' => 'Opis strony',
            'title' => 'Tytuł',
            'deletable' => 'Możliwość usunięcia'
        ];
    }

    public function implementNewModule(Module $module){
        $template = Template::find()->where(['name' => 'new_module'])->one();

        $this->url = "/";
        $this->title = $module->name;
        $this->deletable = 0;
        $this->module_id = $module->id;

        if($template){
            $this->data = $template->html_data;
            $this->template_id = $template->id;
        }

        return $this->save();
    }

    public function getModule(){
        return $this->hasOne(Module::className(), ['id' => 'module_id']);
    }

    public static function getPagesForNavigation($moduleId, $activePageId = null){
        $module = Module::findOne($moduleId);

        if($module->show_default_nav === 1){
            $module = Module::findOne(1);
        }

        $modules = Module::find()->where(['<>','id','1'])->all();
        $pages = Page::find()->where(['active' => 1, 'module_id' => $module->id])->all();
        $atTheEnd = [];
        $atTheBeginning = [];
        $result = [];

        if(!empty($pages)){
            foreach($pages AS $page){
                Page::handlePage($page, $module, $activePageId, 0, $atTheEnd, $result);
            }
        }

        if($module->id === 1){
            if(!empty($modules)){
                foreach($modules AS $module){
                    if($module->add_link_to_nav === 1){
                        $modulePages = Page::find()->where(['active' => 1, 'module_id' => $module->id])->all();

                        foreach($modulePages AS $page){
                            if($page->deletable == 0){
                                Page::handlePage($page, $module, $activePageId, false, $atTheEnd, $result);
                            }
                        }
                    }
                }
            }
        }
        else{
            $page = Page::findOne(1);
            $module = Module::findOne($page->module_id);
            $atTheBeginning[] = Page::handlePage($page, $module, $activePageId, false);
        }

        if(!empty($atTheBeginning)){
            foreach($atTheBeginning AS $page){
                array_unshift($result, $page);
            }
        }

        if(!empty($atTheEnd)){
            foreach($atTheEnd AS $page){
                $result[] = $page;
            }
        }


        return $result;
    }

    private static function handlePage($page, $module, $activePageId, $staticPage = 0, &$atTheEnd = [], &$result = []){
        $template = Template::findOne($page->template_id);
        $page->staticLink = $staticPage;

        if($page->show_in_navigation === 1){
            if($activePageId !== null && $page->id === $activePageId){
                $page->activeLink = true;
            }
            if($page->url !== "/"){
                if($module->url !== "/"){
                    $page->url = $module->url.$page->url.".html";
                }
                else{
                    $page->url = $page->url.".html";
                }
            }
            else{
                if($module->id !== 1){
                    $page->url = $module->url.".html";
                }
            }

            if($template->at_the_end === 1){
                $atTheEnd[] = $page;
            }
            else{
                $result[] = $page;
            }

            return $page;
        }

        return null;
    }
}