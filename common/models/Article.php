<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;

    class Article extends ActiveRecord{

        public function rules(){
            return [
                [['title', 'lead', 'text', 'image'], 'filter', 'filter' => 'trim'],
                [['title', 'lead', 'text', 'image'], 'required'],

                ['title', 'string', 'max' => 255],
            ];
        }

        public function attributeLabels(){
            return [
                'title' => 'Tytuł',
                'lead' => 'Wstep',
                'text' => "Treść",
                'image' => 'Główny obrazek',
                'created' => 'Utworzono',
                'article_category_id' => 'Kategoria',
            ];
        }

        public function getUser(){
            return $this->hasOne(User::className(), ['id' => 'user_id']);
        }
        public function getCategory(){
            return $this->hasOne(ArticleCategory::className(), ['id' => 'article_category_id']);
        }

        public function beforeSave($insert){
            $this->user_id = Yii::$app->user->identity->id;

            return parent::beforeSave($insert);
        }
    }