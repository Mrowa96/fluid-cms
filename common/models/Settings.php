<?php
    namespace common\models;

    use fluid\IUpdater;
    use Yii;
    use yii\helpers\Json;

    class Settings extends \yii\db\ActiveRecord{

        public $primaryKey = "key";

        public static function tableName(){
            return '{{%settings}}';
        }

        public function rules(){
            return [
                [['key', 'value'], 'required'],
                [['description'], 'string'],
                [['key', 'value'], 'string', 'max' => 256]
            ];
        }

        public function attributeLabels(){
            return [
                'id' => 'ID',
                'key' => 'Właściwość',
                'family' => 'Rodzina',
                'value' => 'Wartość',
                'description' => 'Opis',
            ];
        }

        public function updateSystem(IUpdater $updater){
            if(property_exists($updater, "newestVersion") && $updater->newestVersion !== null){
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();

                try {
                    $id = Settings::findOne(['key' => 'version_id']);
                    $prevId = Settings::findOne(['key' => 'version_previous_id']);

                    $prevId->value = $id->value;
                    $id->value = $updater->newestVersion->id;

                    if($prevId->value !== $id->value){
                        $prevId->save();
                        $id->save();

                        $created = Settings::findOne(['key' => 'version_create']);
                        $created->value = $updater->newestVersion->created;
                        $created->save();

                        $number = Settings::findOne(['key' => 'version_number']);
                        $number->value = $updater->newestVersion->version;
                        $number->save();

                        $transaction->commit();

                        return true;
                    }
                    else{
                        throw new \Exception("Cannot install the same update.");
                    }
                }
                catch(\Exception $e) {
                    $transaction->rollback();

                    return $e->getMessage();
                }
            }
            else{
                return "Could not read update information.";
            }
        }

        public function rollbackUpdate(IUpdater $updater){
            $prevId = (int) Settings::findOne(['key' => 'version_previous_id'])->value;
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();

            if($prevId !== 0){
                $update = Json::decode($updater->information($prevId), false);

                try {
                    $id = Settings::findOne(['key' => 'version_id']);
                    $id->value = $update->id;
                    $id->save();

                    $created = Settings::findOne(['key' => 'version_create']);
                    $created->value = $update->created;
                    $created->save();

                    $number = Settings::findOne(['key' => 'version_number']);
                    $number->value = $update->version;
                    $number->save();

                    $prev = Settings::findOne(['key' => 'version_previous_id']);
                    if($update->previous !== null && property_exists($update->previous, "id")){
                        $prev->value = $update->previous->id;
                    }
                    else{
                        $prev->value = 0;
                    }
                    $prev->save();

                    $transaction->commit();

                    return true;
                }
                catch(\Exception $e) {
                    $transaction->rollback();

                    return $e->getMessage();
                }
            }
            else{
                try {
                    $id = Settings::findOne(['key' => 'version_id']);
                    $id->value = Settings::findOne(['key' => 'version_default_id'])->value;
                    $id->save();

                    $created = Settings::findOne(['key' => 'version_create']);
                    $created->value = Settings::findOne(['key' => 'version_default_create'])->value;
                    $created->save();

                    $number = Settings::findOne(['key' => 'version_number']);
                    $number->value = Settings::findOne(['key' => 'version_default_number'])->value;
                    $number->save();

                    $transaction->commit();

                    return true;
                }
                catch(\Exception $e) {
                    $transaction->rollback();

                    return $e->getMessage();
                }
            }
        }

        public static function getVersionInfo(){
            return [
                'id' => Settings::findOne(['key' => 'version_id'])->value,
                'created' => Settings::findOne(['key' => 'version_create'])->value,
                'number' => Settings::findOne(['key' => 'version_number'])->value,
            ];
        }
    }
