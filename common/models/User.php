<?php
    namespace common\models;

    use Yii;
    use yii\base\NotSupportedException;
    use yii\behaviors\TimestampBehavior;
    use yii\db\ActiveRecord;
    use yii\web\IdentityInterface;

    class User extends ActiveRecord implements IdentityInterface{
        const STATUS_DELETED = 0;
        const STATUS_ACTIVE = 10;

        public $password;
        public $password_repeat;

        public static function tableName(){
            return '{{%user}}';
        }

        public function behaviors(){
            return [
                TimestampBehavior::className(),
            ];
        }

        public function rules(){
            return [
                ['status', 'default', 'value' => self::STATUS_ACTIVE],
                ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ];
        }

        public function beforeSave($insert){
            unset($this->role);
            unset($this->roleName);

            return parent::beforeSave($insert);
        }

        public function afterFind(){
            $auth = Yii::$app->authManager;
            $roles = $auth->getRolesByUser($this->id);
            reset($roles);

            $this->created_at = date("M, Y", $this->created_at);
            $this->role = $roles[key($roles)]->name;
            $this->roleName = $roles[key($roles)]->description;

            parent::afterFind();
        }

        public function attributes(){
            return array_merge(
                parent::attributes(),
                ['role', 'roleName']
            );
        }

        public function attributeLabels(){
            return [
                'username' => 'Nazwa użytkownika',
                'password' => 'Hasło',
                'password_repeat' => 'Powtórz hasło',
                'role' => 'Rola',
                'roleName' => 'Rola',
                'sex' => 'Płeć',
                'created_at' => 'Utworzony'
            ];
        }


        public static function findIdentity($id){
            return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
        }

        public static function findIdentityByAccessToken($token, $type = null){
            throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
        }

        public static function findByUsername($username){
            return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        }

        public static function findByPasswordResetToken($token){
            if (!static::isPasswordResetTokenValid($token)) {
                return null;
            }

            return static::findOne([
                'password_reset_token' => $token,
                'status' => self::STATUS_ACTIVE,
            ]);
        }

        public static function isPasswordResetTokenValid($token){
            if (empty($token)) {
                return false;
            }

            $timestamp = (int) substr($token, strrpos($token, '_') + 1);
            $expire = Yii::$app->params['user.passwordResetTokenExpire'];
            return $timestamp + $expire >= time();
        }

        public function getId(){
            return $this->getPrimaryKey();
        }

        public function getAuthKey(){
            return $this->auth_key;
        }

        public function validateAuthKey($authKey){
            return $this->getAuthKey() === $authKey;
        }

        public function validatePassword($password){
            return Yii::$app->security->validatePassword($password, $this->password_hash);
        }

        public function setPassword($password){
            $this->password_hash = Yii::$app->security->generatePasswordHash($password);
        }

        public function generateAuthKey(){
            $this->auth_key = Yii::$app->security->generateRandomString();
        }

        public function generatePasswordResetToken(){
            $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
        }

        public function removePasswordResetToken(){
            $this->password_reset_token = null;
        }

        public static function getAvatars(){
            //to rewrite
            $avatars = [
                Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/men1.png",
                Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/men2.png",
                Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/men3.png",
                Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/men4.png",
                Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/men5.png",
                Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/woman1.png",
                Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/woman2.png",
                Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/woman3.png",
                Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/woman4.png",
                Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/woman5.png",
            ];

            return $avatars;
        }
    }
