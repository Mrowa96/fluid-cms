<?php
    namespace common\models;

    use yii\db\ActiveRecord;

    class Visit extends ActiveRecord{

        public function rules(){
            return [
                ['date', 'required'],
                ['ip', 'required'],
            ];
        }

        public static function fetchByMonth(){
            $result = [
                'months' => [],
                'am' => 0,
                'pm' => 0
            ];
            $dates = Visit::find()->all();

            for($i = 0; $i< 12; $i++){
                $result['months'][$i] = 0;
            }

            if(!empty($dates)){
                foreach($dates AS $date){
                    $d = getdate(strtotime($date['date']));

                    if($d['hours'] >= 0 && $d['hours'] <= 12){
                        $result['am']++;
                    }
                    else{
                        $result['pm']++;
                    }

                    $result['months'][$d['mon'] - 1]++;
                }
            }

            return $result;
        }
    }