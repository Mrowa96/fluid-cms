<?php
    namespace backend\assets;

    use yii\web\AssetBundle;

    class AppAsset extends AssetBundle{
        public $basePath = '@webroot';
        public $baseUrl = '@web';
        public $css = [
            'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
            "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            CDN."/font-awesome/css/font-awesome.min.css",
            CDN.'/fluid-cms/styles/common/app.css',
            CDN.'/fluid-cms/styles/backend/admin.css',
            CDN.'/fluid-cms/styles/backend/skin.css',
            CDN.'/fluid-elements/styles/app.css',
        ];
        public $js = [
            "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js",
            CDN.'/fluid-elements/scripts/fluid-core.js',
            CDN.'/fluid-cms/scripts/backend/admin.js',
            CDN.'/fluid-cms/scripts/backend/_slimScroll/jquery.slimscroll.min.js',
            CDN.'/fluid-cms/scripts/backend/_chartjs/Chart.min.js',
            CDN.'/fluid-cms/scripts/backend/_fluid-elements/fluid-instance.js',
            CDN.'/fluid-cms/scripts/backend/yii_overrides.js',
        ];
        public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
        public $depends = [
            'yii\web\YiiAsset',
        ];


    }
