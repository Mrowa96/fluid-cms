<?php
    use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="box">
    <div class="box-content">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'url',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function($url){
                            return '<a href="'.$url.'" title="Pokaż" class="action glyphicon glyphicon-eye-open"></a>';
                        },
                        'update' => function($url){
                            return '<a href="'.$url.'" title="Edytuj" class="action glyphicon glyphicon-pencil"></a>';
                        },
                        'delete' => function ($url, $model) {
                            if($model->deletable === 1){
                                return '<a href="'.$url.'" title="Usuń" class="action glyphicon glyphicon-trash" data-confirm="Jesteś pewny, ze chcesz usunać ten obiekt?" data-method="post"></a>';
                            }
                            else{
                                return "";
                            }
                        }
                    ]
                ],

            ],
            'layout'=>"{items}\n{pager}",
        ]); ?>
    </div>
    <div class="box-footer">
        <?= Html::a("Utwórz moduł", ['create'], ['class' => 'btn btn-success']); ?>
    </div>
</div>
