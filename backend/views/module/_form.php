<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<div class="module-form">
    <?php $form = ActiveForm::begin([
        'id' => 'user-form',
        'enableClientValidation' => true,
    ]); ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'url') ?>
    <?= $form->field($model, 'active')->checkbox() ?>
    <?php if($model->id !== 1): ?>
        <?= $form->field($model, 'add_link_to_nav')->checkbox() ?>
        <?= $form->field($model, 'show_default_nav')->checkbox() ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? "Utwórz" : "Zaktuzalizuj", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
