<div class="module-create box box-form">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
