<?php
    use backend\assets\AppAsset;
    use yii\helpers\Html;
    use common\widgets\Alert;
    use yii\widgets\Breadcrumbs;

    AppAsset::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>

    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <?= Html::csrfMetaTags() ?>

            <title><?= Html::encode($this->title) ?></title>
            <?php $this->head() ?>
        </head>
        <body class="hold-transition skin-purple-light">
            <?php $this->beginBody() ?>

            <div class="wrapper auth">
                <div class="content-wrapper">
                    <?= Alert::widget() ?>

                    <section class="content-header">
                        <h1>
                            <?php
                                if(isset($this->contentTitle)){
                                    echo $this->contentTitle;
                                }
                            ?>
                        </h1>
                    </section>

                    <?php
                        if(isset($content)){
                            echo $content;
                        }
                    ?>
                </div>

                <?= $this->render("@backend/views/partials/footer"); ?>
            </div>
            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>


