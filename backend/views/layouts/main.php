<?php
    use backend\assets\AppAsset;
    use yii\helpers\Html;
    use common\widgets\Alert;
    use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>

    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <?= Html::csrfMetaTags() ?>

            <title><?= Html::encode($this->title) ?></title>
            <?php $this->head() ?>
        </head>
        <body class="hold-transition skin-purple-light">
            <?php $this->beginBody() ?>

            <div class="wrapper" >
                <?= $this->render("@backend/views/partials/header"); ?>
                <?= $this->render("@backend/views/partials/sidebar"); ?>
                <?= $this->render("@common/views/partials/loadScreen"); ?>
                <?= $this->render("@common/views/partials/fluidCmsVer"); ?>

                <div class="content-wrapper">
                    <?= Alert::widget() ?>

                    <section class="content-header">
                        <h1><?= $this->title; ?></h1>

                        <?= Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            'homeLink' => [
                                'label' => ''
                            ]
                        ]) ?>
                    </section>

                    <div class="clearFloat"></div>

                    <section class="content">
                        <?php
                            if(isset($content)){
                                echo $content;
                            }
                        ?>

                        <?php if(isset($this->params['additionalData']) && !empty($this->params['additionalData'])):?>
                            <div class="additionalSections">
                                <?php
                                    foreach($this->params['additionalData'] AS $data){
                                        if(isset($data['view'])){
                                            $params = (isset($data['params'])) ? $data['params'] : [];

                                            echo $this->render($data['view'], $params);
                                        }
                                    }
                                ?>
                            </div>
                        <?php endif; ?>
                    </section>
                </div>
                <?= $this->render("@backend/views/partials/footer"); ?>
            </div>
            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>


