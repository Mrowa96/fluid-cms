<div class="box">
    <div class="box-content settings-box">
        <div data-fluid="element: tabbedView, default: first">
            <ul class="controls">
                <li data-trigger="updates">Aktualizacje</li>
            </ul>
            <div class="sections">
                <section data-section="updates">
                    <div class="informations">
                        <p>
                            Używasz aktualnie Fluid CMS &copy; <b><?= $versionInfo['number']; ?></b> z dnia <?= $versionInfo['created']; ?>
                        </p>
                    </div>
                    <span class="btn btn-warning" id="checkUpdate">Sprawdź aktualizację</span>
                </section>
            </div>
        </div>

        <script>
            window.addEventListener("load", function(){
                FI.Lib.handleUpdates({
                    element: document.querySelector("#updatesSection")
                });
            });
        </script>
    </div>
</div>