<?php
    use yii\helpers\Url;

    $user = Yii::$app->user->identity;
?>

<header class="main-header">
    <a href="<?= Url::home(); ?>" class="logo hidden-xs">
        <span class="logo-mini">
            <i class="fa fa-dashboard"></i>
        </span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle hidden-lg hidden-md" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $user->avatar; ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs">
                            <?= $user->username;?>
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="<?= $user->avatar; ?>" class="img-circle" alt="User Image">
                            <p>
                                <?= $user->username; ?>
                                <small>Dołączył <?= $user->created_at; ?></small>
                            </p>
                        </li>

                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= Url::toRoute(["user/view", 'id' => Yii::$app->user->identity->id]); ?>" class="btn btn-default btn-flat">Profil</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?= Url::toRoute("/logout"); ?>" class="btn btn-default btn-flat">Wyloguj</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <?php /*<li>
                    <a href="<?= Url::toRoute(["mail/index"]); ?>">
                        <i class="fa fa-envelope"></i>
                    </a>
                </li>*/ ?>
                <li>
                    <a href="<?= Yii::getAlias("@frontendLink"); ?>">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
            </ul>
        </div>

    </nav>
</header>