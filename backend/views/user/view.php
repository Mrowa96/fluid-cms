<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;
?>
<div class="box user-profile">
    <div class="box-header">
        <img src="<?= $model->avatar;?>" class="img-responsive img-circle center-block">
    </div>

    <div class="box-content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'username',
                'email:email',
                [
                    'label' => 'Płeć',
                    'value' => ($model->sex == "men") ? "Mężczyzna" : "Kobieta",
                ],
                'roleName',
                'created_at',
            ],
        ]) ?>
    </div>
    <?php if($this->params['isAdmin'] === true): ?>
        <div class="box-footer">
            <?= Html::a("Zaktualizuj", ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a("Usuń", ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => "Jesteś pewny że chcesz usunąć tego użytkownika?",
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    <?php endif; ?>
</div>
