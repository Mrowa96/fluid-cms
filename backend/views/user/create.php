<div class="user-create box box-form">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'roles' => $roles,
            'sex' => $sex,
            'avatars' => $avatars,
        ]) ?>
    </div>
</div>
