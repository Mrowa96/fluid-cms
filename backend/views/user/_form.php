<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>
<div class="user-form">
    <?php $form = ActiveForm::begin([
        'id' => 'user-form',
        'enableAjaxValidation' => true,
      //  'enableClientValidation' => true
    ]); ?>
    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'password_repeat')->passwordInput() ?>
    <?php //if($model->isNewRecord): ?>
        <?= $form->field($model, 'role')
            ->dropDownList(
                $roles,
                ['prompt'=>'Wybierz role']
            );
        ?>
    <?php //endif; ?>
    <?= $form->field($model, 'sex')
        ->dropDownList(
            $sex,
            ['prompt'=>'Wybierz płeć']
        );
    ?>
    <div class="form-group" id="avatarsBlock">
        <?= $form->field($model, 'avatar')->hiddenInput() ?>

        <ul class="avatars-list">
            <?php if(!empty($avatars)): ?>
                <?php foreach($avatars AS $avatar): ?>
                    <li>
                        <img src="<?= $avatar; ?>" alt="Avatar" class="img-responsive">
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>

        <script>
            window.addEventListener("load", function(){
                FI.Lib.selectAvatar({
                    avatars: document.querySelector("#avatarsBlock").querySelectorAll(".avatars-list")[0],
                    input: document.querySelector("#avatarsBlock").getElementsByTagName("input")[0]
                });
            }, false);
        </script>

    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? "Utwórz" : "Zaktualizuj", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
