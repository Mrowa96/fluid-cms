<?php
    use yii\helpers\Url;
?>

<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Liczba modułów</span>
                <span class="info-box-number"><?= $moduleCount; ?></span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-list-alt"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Liczba szablonów</span>
                <span class="info-box-number"><?= $templateCount; ?></span>
            </div>
        </div>
    </div>

    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-sitemap"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Liczba stron</span>
                <span class="info-box-number"><?= $pageCount; ?></span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Ilość odwiedzin</span>
                <span class="info-box-number"><?= $visitCount; ?></span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Statystyki odwiedzin</h3>
                <div class="box-tools pull-right">
                    <a href="<?= Url::toRoute(["/refresh-visitors"]); ?>" class="btn btn-box-tool"
                       title="Wyczyść statystyki">
                        <i class="fa fa-spin fa-refresh"></i>
                    </a>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <p class="text-center">
                            <strong>Odwiedziny w roku 2016</strong>
                        </p>
                        <div class="chart">

                            <canvas id="salesChart" style="height: 180px;"></canvas>
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div class="info-box bg-yellow">
                            <span class="info-box-icon"><i class="fa fa-sun-o"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Przed południem</span>
                                <span class="info-box-number" id="visitsAm">0</span>
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                                      <span class="progress-description">
                                        Ilość odiwedzin strony w godzinach 24-12.
                                      </span>
                            </div>
                        </div>
                        <div class="info-box bg-red">
                            <span class="info-box-icon"><i class="fa fa-moon-o"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Po południu</span>
                                <span class="info-box-number" id="visitsPm">0</span>
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                                  <span class="progress-description">
                                    Ilość odiwedzin strony w godzinach 12-24.
                                  </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-12">

        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Użytkownicy</h3>
                <div class="box-tools pull-right">
                    <span class="label label-danger"><?= count($users); ?> <?= (count($users) > 1) ? "Użytkowników" : "Użytkownik"; ?></span>
                </div>
            </div>
            <div class="box-body no-padding">
                <ul class="users-list clearfix">
                    <?php if (isset($users) && !empty($users)): ?>
                        <?php foreach ($users AS $user): ?>
                            <li>
                                <a class="users-list-avatar" href="<?= Url::toRoute(["/user/view", 'id' => $user->id]); ?>">
                                    <img src="<?= $user->avatar; ?>" alt="User Avatar">
                                </a>

                                <a class="users-list-name" href="<?= Url::toRoute(["/user/view", 'id' => $user->id]); ?>">
                                    <?= $user->username; ?>
                                </a>
                                <span class="users-list-date"><?= $user->created_at; ?></span>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="box-footer text-center">
                <a href="<?= Url::toRoute("/user"); ?>" class="uppercase">Wyświetl wszystkich</a>
            </div>
        </div>
    </div>
</div>