<div class="article-create box box-form">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'categories' => $categories
        ]) ?>
    </div>
</div>
