<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJsFile(Yii::getAlias("@cdnLink")."/tinymce/tinymce.min.js");
?>

<div class="article-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lead')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'article_category_id')
        ->dropDownList(
            $categories,
            ['prompt'=>'Wybierz kategorię']
        );
    ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Zapisz', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <script>
        window.addEventListener("load", function(){
            var fileManager = new Fluid.FileManager({
                mode: "input",
                modeOptions: {
                    input: document.querySelector("#article-image"),
                    displayAs: "image"
                },
                filter: "image",
                create: true
            });
            FI.Lib.loadTinyMCE(document.querySelector("#article-text"));
        });
    </script>
    <?php ActiveForm::end(); ?>
</div>