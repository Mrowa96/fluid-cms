<?php
    use yii\grid\GridView;
    use yii\bootstrap\Html;
?>
<section class="content-header">
    <h1>Kategorie artykułów</h1>
</section>
<div class="box-wrap-ad col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="box">
        <div class="box-content">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view-category} {update-category} {delete-category}',
                        'buttons' => [
                            'view-category' => function($url){
                                return '<a href="'.$url.'" title="Pokaż" class="action glyphicon glyphicon-eye-open"></a>';
                            },
                            'update-category' => function($url){
                                return '<a href="'.$url.'" title="Edytuj" class="action glyphicon glyphicon-pencil"></a>';
                            },
                            'delete-category' => function ($url, $model) {
                                if($model->deletable) {
                                    return '<a href="' . $url . '" title="Usuń" class="action glyphicon glyphicon-trash" data-confirm="Jesteś pewny, ze chcesz usunać ten obiekt?" data-method="post"></a>';
                                }
                            }
                        ]
                    ],
                ],
                'layout'=>"{items}\n{pager}",
            ]); ?>
        </div>
        <div class="box-footer">
            <?= Html::a("Utwórz kategorię", ['create-category'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>
</div>