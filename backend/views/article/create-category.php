<div class="article-create box box-form">
    <div class="box-content">
        <?= $this->render('_form-category', [
            'model' => $model,
        ]) ?>
    </div>
</div>
