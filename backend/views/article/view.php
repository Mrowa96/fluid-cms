<?php
    use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<div class="box">
    <div class="box-content">
        <div class="template-view">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'label' => 'Obrazek',
                        'value' => '<img src="'.$model->image.'" class="img-responsive">',
                        'format' => 'html'
                    ],
                    'title',
                    'lead',
                    [
                        'label' => 'Treść',
                        'value' => $model->text,
                        'format' => 'html'
                    ],
                    'created',
                    [
                        'label' => 'Autor',
                        'value' => $model->user->username
                    ],
                ],
            ]) ?>
        </div>
    </div>
    <div class="box-footer">
        <?= Html::a('Zaktualizuj', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Jesteś pewny ze chcesz usunąć ten szablon?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
</div>

