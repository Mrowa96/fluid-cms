<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="box">
    <div class="box-content">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                [
                    'attribute' => 'category.name',
                    'label' => 'Kategoria'
                ],
                'user.username',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function($url){
                            return '<a href="'.$url.'" title="Pokaż" class="action glyphicon glyphicon-eye-open"></a>';
                        },
                        'update' => function($url){
                            return '<a href="'.$url.'" title="Edytuj" class="action glyphicon glyphicon-pencil"></a>';
                        },
                        'delete' => function ($url) {
                            return '<a href="'.$url.'" title="Usuń" class="action glyphicon glyphicon-trash" data-confirm="Jesteś pewny, ze chcesz usunać ten obiekt?" data-method="post"></a>';
                        }
                    ]
                ],
            ],
            'layout'=>"{items}\n{pager}",
        ]); ?>
    </div>
    <div class="box-footer">
        <?= Html::a("Napisz artykuł", ['create'], ['class' => 'btn btn-success']); ?>
    </div>
</div>

