<?php
    use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="box">
    <div class="box-content">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                'url:url',
                'module.name',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {active} {update} {delete}',
                    'buttons' => [
                        'view' => function($url){
                            return '<a href="'.$url.'" title="Pokaż" class="action glyphicon glyphicon-eye-open"></a>';
                        },
                        'update' => function($url){
                            return '<a href="'.$url.'" title="Edytuj" class="action glyphicon glyphicon-pencil"></a>';
                        },
                        'delete' => function ($url, $model) {
                            if($model->deletable === 1){
                                return '<a href="'.$url.'" title="Usuń" class="action glyphicon glyphicon-trash" data-confirm="Jesteś pewny, ze chcesz usunać ten obiekt?" data-method="post"></a>';
                            }
                            else{
                                return "";
                            }
                        },
                        'active' => function ($url, $data) {
                            if($data->active){
                                $className = "fa fa-toggle-on";
                                $title = "Dezaktywuj";
                                $url .= '&active=0';
                            }
                            else{
                                $className = "fa fa-toggle-off";
                                $title = "Aktywuj";
                                $url .= '&active=1';
                            }

                            return Html::a(
                                '',
                                $url,
                                [
                                    'class' => 'action '.$className,
                                    'title' => $title,
                                ]
                            );
                        },
                    ],
                ],
            ],
            'layout'=>"{items}\n{pager}",
        ]); ?>
    </div>
    <div class="box-footer">
        <?= Html::a("Utwórz stronę", ['create'], ['class' => 'btn btn-success']); ?>
    </div>
</div>
