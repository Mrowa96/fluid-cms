<div data-fluid="element: editor, <?php echo ($model->id) ? "id: ".$model->id : ""; ?>">
    <div data-fluid="element: dynamicView, startPage: 0 , handleByYourself: false">
        <section data-type="section" data-step="start">
            <div class="text">
                <p>Witamy w kreatorze strony.</p>
                <p>Kliknij przycisk "Dalej", by przejść do następnego kroku.</p>
            </div>
        </section>
        <section data-type="section" data-step="information">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row contentRow">
                            <div class="col-lg-5 col-xs-12 info">
                                <p class="text">Uzupełnij podstawowe informacje: </p>
                                <div id="formInfoContainer"></div>
                            </div>

                            <div class="col-lg-5 col-lg-offset-2 col-xs-12 info2">
                                <p class="text">Wybierz odpowiedni moduł oraz szablon: </p>
                                <div id="formInfo2Container"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section data-type="section" data-step="create">
            <div class="container">
                <div id="templateWindow"></div>
            </div>
        </section>
    </div>
</div>