<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->registerJsFile(Yii::getAlias("@cdnLink")."/fluid-cms/scripts/backend/_codemirror/codemirror.js");
    $this->registerJsFile(Yii::getAlias("@cdnLink")."/fluid-cms/scripts/backend/_codemirror/xml.js");
    $this->registerJsFile(Yii::getAlias("@cdnLink")."/fluid-cms/scripts/backend/_codemirror/htmlmixed.js");
    $this->registerJsFile(Yii::getAlias("@cdnLink")."/fluid-cms/scripts/backend/_codemirror/css.js");
    $this->registerJsFile(Yii::getAlias("@cdnLink")."/fluid-cms/scripts/backend/_codemirror/javascript.js");
    $this->registerCssFile(Yii::getAlias("@cdnLink")."/fluid-cms/styles/backend/_codemirror/codemirror.css");
?>

<div class="template-form">
    <?php $form = ActiveForm::begin(['id' => 'template-form']); ?>
    <div data-fluid="element: tabbedView, default: general">
        <ul class="controls">
            <li data-trigger="general">Ogólne</li>
            <li data-trigger="html_data" data-selector="#template-html_data">Edytor HTML</li>
            <li data-trigger="css_data" data-selector="#template-css_data">Edytor CSS</li>
            <li data-trigger="js_data" data-selector="#template-js_data">Edytor JavaScript</li>
        </ul>
        <div class="sections">
            <section data-section="general">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'active')->checkbox() ?>
                <?= $form->field($model, 'at_the_end')->checkbox() ?>
            </section>
            <section data-section="html_data">
                <?= $form->field($model, 'html_data')->textarea() ?>

                <?php if($model->editable === 0): ?>
                    <div class="form-message">
                        Ten szablon jest nieedytowalny, co oznacza, że wprowadzony kod HTML, będzie jedynie widoczny w podglądzie szablonu podczas tworzenia strony.
                        Docelowo, treśc dla użytkownika będzie generowana dynamicznie.
                    </div>
                <?php endif; ?>
            </section>
            <section data-section="css_data">
                <?= $form->field($model, 'css_data')->textarea() ?>
            </section>
            <section data-section="js_data">
                <?= $form->field($model, 'js_data')->textarea() ?>
            </section>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Utwórz' : 'Zaktualizuj', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php if(!$model->isNewRecord): ?>
            <?= Html::submitButton('Zaktualizuj i kontynuuj edycje', ['class' => 'btn btn-info', 'id' => 'returnButton']) ?>
        <?php endif; ?>
    </div>

    <script>
        window.addEventListener("load", function(){
            FI.Lib.loadEditor({
                element: document.querySelector("#template-html_data"),
                type: 'html'
            });
            FI.Lib.loadEditor({
                element: document.querySelector("#template-css_data"),
                type: 'css'
            });
            FI.Lib.loadEditor({
                element: document.querySelector("#template-js_data"),
                type: 'js'
            });
            FI.Lib.submitAndReturn({
                form: document.querySelector("#template-form"),
                button: document.querySelector("#returnButton")
            });

            var controls = document.querySelector("#template-form").querySelector(".controls"),
                lis = Fluid.Lib.toArray(controls.querySelectorAll("li"));

            lis.forEach(function(li){
                if(li.dataset.selector){
                    li.addEventListener("click", function(){
                        var editBox = document.querySelector(li.dataset.selector);

                        if(editBox && editBox.editor){
                            editBox.editor.refresh();
                        }
                    });
                }
            });
        });
    </script>
    <?php ActiveForm::end(); ?>
</div>

