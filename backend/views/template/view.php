<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->registerCss($model->css_data);
    $this->registerJs($model->js_data);
?>
<div class="box">
    <div class="box-content">
        <div class="template-view">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'title',
                    'description',
                    [
                        'label' => 'Podgląd',
                        'value' => "<div class='template-data'>".$model->html_data."</div>",
                        'format' => 'html'
                    ]
                ],
            ]) ?>
        </div>
    </div>
    <div class="box-footer">
        <?= Html::a('Zaktualizuj', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Jesteś pewny ze chcesz usunąć ten szablon?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
</div>

