<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\Url;

    class Group extends ActiveRecord{

        public $url;

        public static function tableName(){
            return '{{%backend_group}}';
        }

        public function afterFind(){
            parent::afterFind();

            if($this->route === "home"){
                $this->url = Url::home();
            }
            else{
                $this->url = Url::toRoute("/".$this->route);
            }
        }
    }
