<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;

    class Template extends ActiveRecord{

        public function rules(){
            return [
                ['name', 'filter', 'filter' => 'trim'],
                ['name', 'required'],
                ['name', 'string', 'min' => 2, 'max' => 255],
                ['name', 'unique', 'targetClass' => '\backend\models\Template', 'message' => 'This name already exists.'],

                ['title', 'filter', 'filter' => 'trim'],
                ['title', 'required'],
                ['title', 'string', 'min' => 2, 'max' => 255],
                ['title', 'unique', 'targetClass' => '\backend\models\Template', 'message' => 'This name already exists.'],

                ['description', 'filter', 'filter' => 'trim'],
                ['description', 'string', 'max' => 255],
                ['description', 'required'],

                ['at_the_end', 'required'],

                [['html_data', 'js_data', 'css_data'], 'string']
            ];
        }

        public function attributeLabels(){
            return [
                'name' => 'Nazwa',
                'title' => 'Nazwa',
                'description' => 'Opis',
                'html_data' => 'Kod HTML',
                'css_data' => 'Kod CSS',
                'js_data' => 'Kod JavaScript',
                'active' => 'Aktywne',
                'at_the_end' => 'Wyświetl strony operte na szablonie na końcu nawigacji'
            ];
        }
    }
