<?php
    namespace backend\models;

    use common\models\User;
    use yii\base\Model;
    use Yii;
    use yii\db\ActiveRecord;

    class UserCreate extends ActiveRecord{

        public $id = null;
        public $username;
        public $email;
        public $password;
        public $password_repeat;
        public $role;
        public $sex;
        public $avatar;
        public $isNewRecord = true;
        public $user;

        public static function tableName(){
            return '{{%user}}';
        }

        public function rules(){
            return [
                ['username', 'filter', 'filter' => 'trim'],
                ['username', 'required'],
                ['username', 'string', 'min' => 2, 'max' => 255],
                ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],

                ['email', 'filter', 'filter' => 'trim'],
                ['email', 'required'],
                ['email', 'email'],
                ['email', 'string', 'max' => 255],
                ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

                ['role', 'required'],
                ['sex', 'required'],

                ['password', 'required'],
                ['password_repeat', 'required'],
                ['password', 'string', 'min' => 6],
                ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],

                ['avatar', 'filter', 'filter' => 'trim'],
            ];
        }

        public function attributeLabels(){
            return User::attributeLabels();
        }

        public function create(){
            if ($this->validate()) {
                $user = new User();
                $user->username = $this->username;
                $user->email = $this->email;
                if(empty($this->avatar)){
                    if($this->sex === "men"){
                        $user->avatar = Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/men5.png";
                    }
                    else{
                        $user->avatar = Yii::getAlias("@cdnLink")."/fluid-cms/images/avatars/woman1.png";
                    }
                }
                else{
                    $user->avatar = $this->avatar;
                }
                $user->sex = $this->sex;
                $user->setPassword($this->password);
                $user->generateAuthKey();

                if ($user->save()) {
                    $auth = Yii::$app->authManager;
                    $role = $auth->getRole($this->role);
                    $auth->assign($role, $user->id);

                    $this->user = $user;

                    return true;
                }
            }

            return null;
        }
    }
