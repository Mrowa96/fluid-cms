<?php
    namespace backend\models;

    use common\models\User;
    use yii\base\Model;
    use Yii;
    use yii\db\ActiveRecord;

    class UserUpdate extends ActiveRecord{

        public $id = null;
        public $username;
        public $email;
        public $password;
        public $password_repeat;
        public $role;
        public $sex;
        public $avatar;
        public $isNewRecord = false;
        private $user = null;

        public function __construct($id = null){
            if($id !== null){
                $this->id = $id;
                $this->user = User::findOne($this->id);
                $this->attributes = User::find()->where(["id" => $this->id])->one()->attributes;
            }

            parent::__construct();
        }

        public static function tableName(){
            return '{{%user}}';
        }

        public function rules(){
            return [
                ['username', 'filter', 'filter' => 'trim'],
                ['username', 'required'],
                ['username', 'string', 'min' => 2, 'max' => 255],
                ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.', 'when' => function($model) {
                    return $model->username !== $this->user->username;
                }],

                ['email', 'filter', 'filter' => 'trim'],
                ['email', 'required'],
                ['email', 'email'],
                ['email', 'string', 'max' => 255],
                ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.', 'when' => function($model) {
                    return $model->email !== $this->user->email;
                }],

                ['role', 'required'],
                ['sex', 'required'],

                ['password', 'required', 'when' => function($model) {
                    return !empty($model->password);
                }, 'whenClient' => "function (attribute, value) {
                    return value.length > 0;
                }"],
                ['password_repeat', 'required', 'when' => function($model) {
                    return !empty($model->password_repeat);
                }, 'whenClient' => "function (attribute, value) {
                    return value.length > 0;
                }"],
                ['password', 'string', 'min' => 6],
                ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],

                ['avatar', 'filter', 'filter' => 'trim'],
            ];
        }

        public function attributeLabels(){
            return User::attributeLabels();
        }

        public function save($runValidation = true, $attributeNames = null){
            $user = $this->user;

            if($this->validate()){
                $user->username = $this->username;
                $user->email = $this->email;
                $user->avatar = $this->avatar;
                $user->sex = $this->sex;
                if(!empty($this->password) && !empty($this->password_repeat)){
                    $user->setPassword($this->password);
                }
                $user->generateAuthKey();

                if ($user->save()) {
                    $auth = Yii::$app->authManager;

                    $roles = $auth->getRolesByUser($user->id);
                    reset($roles);
                    $oldRoleName = key($roles);

                    if($this->role !== $oldRoleName){
                        $role = $auth->getRole($this->role);
                        $oldRole = $auth->getRole($oldRoleName);

                        $auth->revoke($oldRole, $user->id);
                        $auth->assign($role, $user->id);
                    }

                    return true;
                }
            }

            return false;
        }
    }
