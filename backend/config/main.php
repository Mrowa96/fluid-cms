<?php
    $params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'),
        require(__DIR__ . '/params.php')
    );

    $config = [
        'id' => 'app-backend',
        'language' => 'pl_PL',
        'basePath' => dirname(__DIR__),
        'controllerNamespace' => 'backend\controllers',
        'bootstrap' => ['log'],
        'modules' => [],
        'components' => [
            'user' => [
                'identityClass' => 'common\models\User',
                'enableAutoLogin' => true,
                'identityCookie' => [
                    'name' => '_identity',
                    'path' => '/',
                    'domain' => Yii::getAlias("@domainLink"),
                ],
            ],
            'log' => [
                'traceLevel' => YII_DEBUG ? 3 : 0,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                'enableStrictParsing' => false,
                'rules' => [
                    "/" => "site/index",
                    "/login" => "site/login",
                    "/logout" => "site/logout",
                    "/visitors" => "site/visitors",
                    "/refresh-visitors" => "site/refresh-visitors",
                    "/page" => "page/index",
                    "/module" => "module/index",
                    "/settings" => "settings/index",
                    "/settings/update-system/<id:\d+>" => "settings/update-system",
                    "user/profile/<id:\d+>" => "user/view",
                ],
                'suffix' => '.html'
            ],
            'i18n' => [
                'translations' => [
                    'admin*' => [
                        'class' => 'yii\i18n\PhpMessageSource',
                        //'basePath' => '@app/messages',
                        //'sourceLanguage' => 'en-US',
                        'fileMap' => [
                            'app' => 'app.php',
                            'app/error' => 'error.php',
                        ],
                    ],
                ],
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
        ],
        'params' => $params,
    ];

    if (!YII_ENV_TEST) {
        $config['bootstrap'][] = 'debug';
        $config['modules']['debug'] = [
            'class' => 'yii\debug\Module',
        ];

        $config['bootstrap'][] = 'gii';
        $config['modules']['gii'] = [
            'class' => 'yii\gii\Module',
        ];
    }

    return $config;