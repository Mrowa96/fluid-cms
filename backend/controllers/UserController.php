<?php
    namespace backend\controllers;

    use backend\models\UserCreate;
    use backend\models\UserUpdate;
    use Yii;
    use common\models\User;
    use backend\models\UserSearch;
    use yii\filters\AccessControl;
    use yii\helpers\ArrayHelper;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;
    use yii\web\Response;
    use yii\widgets\ActiveForm;

    class UserController extends BaseController{
        public function behaviors(){
            return [
                'verbs' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'actions' => ['view'],
                            'allow' => true,
                            'roles' => ["editor"]
                        ],
                        [
                            'allow' => true,
                            'roles' => ['admin'],
                        ],
                    ],
                ],
            ];
        }

        public function actionIndex(){
            $this->view->title = "Użytkownicy";
            $this->view->params['breadcrumbs'][] = $this->view->title;
            $searchModel = new UserSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        public function actionView($id){
            $model = $this->findModel($id);

            $this->view->title = "Profil użytkownika";
            $this->view->params['breadcrumbs'][] = ['label' => "Użytkownicy", 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            return $this->render('view', [
                'model' => $model,
            ]);
        }
        public function actionCreate(){
            $model = new UserCreate();

            $roles = Yii::$app->authManager->getRoles();
            $roles = ArrayHelper::map($roles, 'name', 'description');
            $avatars = User::getAvatars();
            $sex = ['men' => 'Mężyczyzna', 'women' => 'Kobieta'];

            $this->view->title = 'Utwórz użytkownika';
            $this->view->params['breadcrumbs'][] = ['label' => 'Użytkownicy', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            }

            if ($model->load(Yii::$app->request->post()) && $model->create()) {
                return $this->redirect(['view', 'id' => $model->user->id]);
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                    'roles' => $roles,
                    'sex' => $sex,
                    'avatars' => $avatars,
                ]);
            }
        }
        public function actionUpdate($id){
            $model = new UserUpdate($id);

            $roles = Yii::$app->authManager->getRoles();
            $roles = ArrayHelper::map($roles, 'name', 'description');
            $avatars = User::getAvatars();
            $sex = ['men' => 'Mężyczyzna', 'women' => 'Kobieta'];

            $this->view->title ='Edycja użytkownika';
            $this->view->params['breadcrumbs'][] = ['label' => "Użytkownicy", 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            }

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                    'roles' => $roles,
                    'sex' => $sex,
                    'avatars' => $avatars,
                ]);
            }
        }
        public function actionDelete($id){
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
        protected function findModel($id){
            if (($model = User::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
