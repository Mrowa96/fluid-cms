<?php
    namespace backend\controllers;

    use common\models\Settings;
    use fluid\Updater;
    use Yii;
    use yii\helpers\Json;

    class SettingsController extends BaseController{

        public function actionIndex(){
            $this->view->title = "Ustawienia";

            return $this->render('index', [
                'versionInfo' => Settings::getVersionInfo()
            ]);
        }

        public function actionCheckUpdate(){
            $updater = new Updater();
            $response = [];

            try{
                $updater->check();
                $response = $updater->newestVersion;
            }
            catch(\Exception $ex){
                $response = $ex->getMessage();
            }

            return Json::encode($response);
        }

        public function actionUpdateSystem($id){
            $updater = new Updater();
            $updater->update((int) $id);
        }

        public function actionRollbackUpdate(){
            $updater = new Updater();
            $updater->rollback();
        }
    }
