<?php
    namespace backend\controllers;

    use Yii;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;
    use common\models\ArticleCategory;
    use common\models\Article;

    class ArticleController extends BaseController{
        public function behaviors(){
            return [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['post'],
                    ],
                ],
            ];
        }

        public function actionIndex(){
            $dataProvider = new ActiveDataProvider([
                'query' => Article::find(),
            ]);
            $articleCategoryDataProvider = new ActiveDataProvider([
                'query' => ArticleCategory::find(),
            ]);

            $this->view->title = Yii::t('admin', 'Artykuły');
            $this->view->params['breadcrumbs'][] = $this->view->title;
            $this->view->params['additionalData'] = [
                [
                    'view' => '@backend/views/article/articleCategoryGrid',
                    'params' => [
                        'dataProvider' => $articleCategoryDataProvider,
                    ]
                ]
            ];

            return $this->render('index', [
                'dataProvider' => $dataProvider
            ]);
        }
        public function actionView($id){
            $this->view->title = Yii::t('admin', 'Podgląd artykułu');
            $this->view->params['breadcrumbs'][] = ['label' => 'Artykuły', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
        public function actionCreate(){
            $model = new Article();
            $articleCategory = new ArticleCategory();
            $categories = $articleCategory->getForForm();

            $this->view->title = Yii::t('admin', 'Napisz artykuł');
            $this->view->params['breadcrumbs'][] = ['label' => 'Artykuły', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                    'categories' => $categories
                ]);
            }
        }
        public function actionUpdate($id){
            $model = $this->findModel($id);
            $articleCategory = new ArticleCategory();
            $categories = $articleCategory->getForForm();

            $this->view->title = Yii::t('admin', 'Edytuj artykuł');
            $this->view->params['breadcrumbs'][] = ['label' => 'Artykuły', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                    'categories' => $categories
                ]);
            }
        }
        public function actionDelete($id){
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        //categoryArticle
        public function actionCreateCategory(){
            $model = new ArticleCategory();

            $this->view->title = Yii::t('admin', 'Utwórz kategorię');
            $this->view->params['breadcrumbs'][] = ['label' => 'Artykuły', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['index']);
            }
            else {
                return $this->render('create-category', [
                    'model' => $model,
                ]);
            }
        }
        public function actionViewCategory($id){
            $this->view->title = Yii::t('admin', 'Podgląd kategorii');
            $this->view->params['breadcrumbs'][] = ['label' => 'Artykuły', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            return $this->render('view-category', [
                'model' => $this->findModelCategory($id),
            ]);
        }
        public function actionUpdateCategory($id){
            $model = $this->findModelCategory($id);

            $this->view->title = Yii::t('admin', 'Edytuj kategorię');
            $this->view->params['breadcrumbs'][] = ['label' => 'Artykuły', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['index']);
            }
            else {
                return $this->render('update-category', [
                    'model' => $model,
                ]);
            }
        }
        public function actionDeleteCategory($id){
            $this->findModelCategory($id)->delete();

            return $this->redirect(['index']);
        }

        protected function findModel($id){
            if (($model = Article::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        protected function findModelCategory($id){
            if (($model = ArticleCategory::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
