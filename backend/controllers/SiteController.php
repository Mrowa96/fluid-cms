<?php
    namespace backend\controllers;

    use backend\models\LoginForm;
    use backend\models\SettingsForm;
    use backend\models\SignupForm;
    use backend\models\Template;
    use common\models\Module;
    use common\models\Page;
    use common\models\Settings;
    use common\models\User;
    use common\models\Visit;
    use Yii;
    use yii\filters\AccessControl;
    use yii\helpers\Json;
    use yii\web\Controller;
    use yii\filters\VerbFilter;

    class SiteController extends BaseController{

        public function actionIndex(){
            $this->view->title = "Strona główna";
            $this->view->registerJsFile(Yii::getAlias("@cdnLink"."/fluid-cms/scripts/backend/visitors.js"));

            //\Yii::$app->session->setFlash('info', 'This is the message');

            return $this->render('index', [
                'users' => User::find()->where(['status' => User::STATUS_ACTIVE])->all(),
                'moduleCount' => Module::find()->where(['active' => 1])->count(),
                'pageCount' => Page::find()->where(['active' => 1])->count(),
                'templateCount' => Template::find()->where(['active' => 1])->count(),
                'visitCount' => Visit::find()->count()
            ]);
        }
        public function actionLogin(){
            $this->layout = "auth";

            if (!\Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            else {
                return $this->render('login', [
                    'model' => $model,
                ]);
            }
        }
        public function actionLogout(){
            Yii::$app->user->logout();

            $session = Yii::$app->session;
            $session->remove("adminAccess");

            return $this->goHome();
        }
        public function actionRequestPasswordReset(){
            $model = new PasswordResetRequestForm();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->sendEmail()) {
                    Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                    return $this->goHome();
                } else {
                    Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
                }
            }

            return $this->render('requestPasswordResetToken', [
                'model' => $model,
            ]);
        }
        public function actionResetPassword($token){
            try {
                $model = new ResetPasswordForm($token);
            } catch (InvalidParamException $e) {
                throw new BadRequestHttpException($e->getMessage());
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
                Yii::$app->session->setFlash('success', 'New password was saved.');

                return $this->goHome();
            }

            return $this->render('resetPassword', [
                'model' => $model,
            ]);
        }

        public function actionVisitors(){
            $visitors = Visit::fetchByMonth();

            echo Json::encode($visitors);
            exit;
        }

        public function actionRefreshVisitors(){
            Visit::deleteAll();

            return $this->redirect("/");
        }
    }
