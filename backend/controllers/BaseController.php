<?php

    namespace backend\controllers;

    use backend\models\Group;
    use Yii;
    use yii\filters\AccessControl;
    use yii\web\Controller;

    class BaseController extends Controller{

        protected $isAdmin = false;

        public function init(){
            $this->view->title = "Fluid CMS - Management";
            $this->view->params['groups'] = Group::find()->groupBy('position')->all();

            Yii::$app->fluid->checkConnection();

            $user = Yii::$app->user->identity;

            if($user){
                $roles = Yii::$app->authManager->getRolesByUser($user->id);

                if(isset($roles['admin'])){
                    $this->isAdmin = true;
                }
            }

            $this->view->params['isAdmin'] = $this->isAdmin;

            parent::init();
        }

        public function behaviors(){
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'actions' => ['error', 'login', 'signup'],
                            'allow' => true,
                            'roles' => ["?"]
                        ],
                        [
                            'allow' => true,
                            'roles' => ['admin', 'editor'],
                        ],
                    ],
                ],
            ];
        }

        public function actions(){
            return [
                'error' => [
                    'class' => 'common\controllers\ErrorController',
                ],
            ];
        }
    }