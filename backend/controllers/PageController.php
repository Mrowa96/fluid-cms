<?php
    namespace backend\controllers;

    use backend\models\Template;
    use common\models\Module;
    use Yii;
    use common\models\Page;
    use yii\data\ActiveDataProvider;
    use backend\controllers\BaseController;
    use yii\helpers\Json;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;

    class PageController extends BaseController{
        public function behaviors(){
            return [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['post'],
                        'savePage' => ['post']
                    ],
                ],
            ];
        }

        public function beforeAction($action){
            $this->enableCsrfValidation = false;

            return parent::beforeAction($action);
        }

        public function actionIndex(){
            $this->view->title = "Strony";
            $this->view->params['breadcrumbs'][] = $this->view->title;

            $dataProvider = new ActiveDataProvider([
                'query' => Page::find(),
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }
        public function actionCreate(){
            $model = new Page();

            $this->layout = "empty";
            $this->view->registerJsFile(Yii::getAlias("@cdnLink")."/tinymce/tinymce.min.js");

            return $this->render('create', [
                'model' => $model,
            ]);

        }
        public function actionUpdate($id){
            $model = $this->findModel($id);

            $this->layout = "empty";
            $this->view->registerJsFile(Yii::getAlias("@cdnLink")."/tinymce/tinymce.min.js");

            return $this->render('update', [
                'model' => $model,
            ]);
        }
        public function actionDelete($id){
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
        public function actionView($id){
            $page = $this->findModel($id);
            $module = Module::findOne($page->module_id);

            $module->url = str_replace("/", "", $module->url);
            $url = Yii::getAlias("@frontendLink").$module->url.$page->url.".html";

            $this->redirect($url);
        }

        public function actionGetData(){
            $templates = Template::find()->asArray()->all();
            $modules = Module::find()->asArray()->all();
            $data = [
                'templates' => $templates,
                'modules' => $modules,
            ];

            echo Json::encode($data);
            exit;
        }

        public function actionGetExistingData($id){
            $model = $this->findModel($id);
            $response = [];

            foreach($model->attributes AS $key => $value){
                $response[$key] = $value;
            }

            echo Json::encode($response);
            exit;
        }

        public function actionSavePage(){
            $data = Json::decode(Yii::$app->request->post('data'));
            $id = Yii::$app->request->post('id', 0);

            $response = [
                'failed' => 1,
                'message' => 'Wystąpił nieznany błąd.'
            ];

            if(!empty($data)){
                if($id !== 0){
                    $page = Page::findOne($id);
                }
                else{
                    $page = new Page();
                    $page->deletable = 1;
                }

                foreach($data AS $d){
                    $page->{$d['name']} = $d['value'];
                }

                if($page->save()){
                    $response = [
                        'failed' => 0
                    ];
                }
            }

            echo Json::encode($response);
            exit;
        }

        public function actionActive($id, $active){
            $model = $this->findModel($id);
            $active = (int) $active;

            $model->active = $active;
            $model->save();

            return $this->redirect(['index']);
        }

        protected function findModel($id){
            if (($model = Page::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
