<?php
    namespace backend\controllers;

    use Yii;
    use backend\models\Template;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Json;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;

    class TemplateController extends BaseController{
        public function behaviors()
        {
            return [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['post'],
                    ],
                ],
            ];
        }

        public function actionIndex(){
            $this->view->title = Yii::t('admin', 'Szablony');
            $this->view->params['breadcrumbs'][] = $this->view->title;

            $dataProvider = new ActiveDataProvider([
                'query' => Template::find(),
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }

        public function actionView($id){
            $this->view->title = Yii::t('admin', 'Podgląd szablonu');
            $this->view->params['breadcrumbs'][] = ['label' => 'Szablony', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        public function actionCreate(){
            $model = new Template();
            $data = Yii::$app->request->post();

            $this->view->title = Yii::t('admin', 'Utwórz szablon');
            $this->view->params['breadcrumbs'][] = ['label' => 'Szablony', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            if ($model->load($data)) {
                $model->name = $this->toLowerCaseString($data['Template']['title']);
                $model->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        public function actionUpdate($id){
            $model = $this->findModel($id);
            $data = Yii::$app->request->post();

            $this->view->title = Yii::t('admin', 'Edytuj szablon');
            $this->view->params['breadcrumbs'][] = ['label' => 'Szablony', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            if ($model->load($data)) {
                $editAfterSubmit = (isset($data['editAfterSubmit'])) ? $data['editAfterSubmit'] : false;
                unset($data['editAfterSubmit']);

                $model->name = $this->toLowerCaseString($data['Template']['title']);
                $model->save();

                if($editAfterSubmit == 1){
                    return $this->redirect(['update', 'id' => $model->id]);
                }
                else{
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        public function actionDelete($id){
            $model = $this->findModel($id);

            if($model->deletable){
                $model->delete();
            }

            return $this->redirect(['index']);
        }

        protected function findModel($id){
            if (($model = Template::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

        private function toLowerCaseString($string){
            $newString = str_replace(" ", "_", $string);
            $newString = mb_strtolower($newString);

            return $newString;
        }
    }
