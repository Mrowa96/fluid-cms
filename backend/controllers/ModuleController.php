<?php
    namespace backend\controllers;

    use backend\models\Template;
    use common\models\Page;
    use Yii;
    use common\models\Module;
    use yii\data\ActiveDataProvider;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;

    class ModuleController extends BaseController{

        public function actionIndex(){
            $this->view->title = "Moduły";

            $this->view->params['breadcrumbs'][] = $this->view->title;

            $dataProvider = new ActiveDataProvider([
                'query' => Module::find(),
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }
        public function actionView($id){
            $model = $this->findModel($id);

            if($model->url === "/"){
                $this->redirect(Yii::getAlias("@frontendLink"));
            }
            else{
                $this->redirect(Yii::getAlias("@frontendLink").$model->url.".html");
            }
        }
        public function actionCreate(){
            $model = new Module();

            $this->view->title = Yii::t('admin', 'Utwórz moduł');
            $this->view->params['breadcrumbs'][] = ['label' => "Moduły", 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $page = new Page();

                if($page->implementNewModule($model)){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
                else{
                    return $this->redirect(['index']);
                }
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
        public function actionUpdate($id){
            $model = $this->findModel($id);

            $this->view->title = Yii::t('admin', 'Edycja modułu');
            $this->view->params['breadcrumbs'][] = ['label' => "Moduły", 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['index']);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
        public function actionDelete($id){
            $model = $this->findModel($id);

            if($model->deletable){
                $model->delete();
                Page::deleteAll(['module_id' => $id]);
            }

            return $this->redirect(['index']);
        }

        protected function findModel($id){
            if (($model = Module::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
