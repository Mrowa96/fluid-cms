<?php
    namespace backend\controllers;

    use fluid\Mail;
    use Yii;
    use yii\base\ErrorException;

    class MailController extends BaseController{

        public function beforeAction($action){
            $this->redirect(['site/index']);
        }

        public function actionIndex(){
            $this->view->title = "Poczta";

            try{
                $mail = new Mail("imap.gmail.com", 993, "imap/ssl", "mrowa94@gmail.com", "Julita96");
                $mail->listMailboxes();
            }
            catch(ErrorException $ex){
                Yii::$app->session->setFlash('error', 'dsa');
            }

            return $this->render("index");
        }
    }
