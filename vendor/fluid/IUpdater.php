<?php
    namespace fluid;

    interface IUpdater{
        public function check();
        public function update($updateId = null);
        public function rollback();
        public function information($updateId);
    }