<?php
    namespace fluid;

    class Lib{
        public static function concatOnUpper($string, $delimiter = "_", $firstToUpper = false){
            if(is_string($string) && !empty($string)){
                $newString = "";
                $letters = str_split($string);
                $special = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '+', '-', '\\', '/', '\'', '"', '.', ',', '=', '_'];

                foreach($letters AS $key => $letter){
                    if($key !== 0 && !in_array($letter, $special)){
                        if(strtoupper($letter) === $letter){
                            $letter = $delimiter.strtolower($letter);
                        }
                    }

                    $newString .= $letter;
                }

                if($firstToUpper){
                    $newString = ucfirst($newString);
                }

                return $newString;
            }

            return null;
        }

        public static function explodeOnUpper($string){
            if(is_string($string) && !empty($string)){
                $splited = [];
                $letters = str_split($string);
                $special = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '+', '-', '\\', '/', '\'', '"', '.', ',', '=', '_'];

                for($i = count($letters) - 1; $i > 0; $i--){
                    $letter = $letters[$i];

                    if(!in_array($letter, $special)){
                        if(strtoupper($letter) === $letter){
                            $splited = [mb_substr($string, 0, $i), mb_substr($string, $i)];
                            break;
                        }
                    }
                }

                return $splited;
            }

            return null;
        }

        public static function humanBytesFormat($bytes){
            $result = null;
            $bytes = floatval($bytes);
            $arBytes = array(
                0 => array(
                    "unit" => "TB",
                    "value" => pow(1024, 4)
                ),
                1 => array(
                    "unit" => "GB",
                    "value" => pow(1024, 3)
                ),
                2 => array(
                    "unit" => "MB",
                    "value" => pow(1024, 2)
                ),
                3 => array(
                    "unit" => "KB",
                    "value" => 1024
                ),
                4 => array(
                    "unit" => "B",
                    "value" => 1
                ),
            );

            foreach($arBytes as $arItem) {
                if($bytes >= $arItem["value"]) {
                    $result = $bytes / $arItem["value"];
                    $result = str_replace(".", "," , strval(round($result, 2)));

                    return [
                        'size' => $result,
                        'sizeUnit' => $arItem['unit']
                    ];
                }
            }

            return [
                'size' => 0,
                'sizeUnit' => "MB"
            ];
        }
    }