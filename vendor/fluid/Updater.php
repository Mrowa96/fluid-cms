<?php
    namespace fluid;

    use common\models\Settings;
    use yii\helpers\Json;

    class Updater implements IUpdater
    {
        public $response;
        public $newestVersion = null;
        private $url = "http://fc-cdn.localhost/";
        private $checked = false;
        private $package = null;
        private $packagePaths = [];
        private $packageLocation = null;
        private $tempUpdateId = null;

        public function check(){
            $ch = curl_init();
            $url = $this->url . "check-update";

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $this->response = curl_exec($ch);
            $this->newestVersion = Json::decode($this->response, false);
            $this->checked = true;
            curl_close($ch);

            if(property_exists($this->newestVersion, "id")){
                if(Settings::findOne(['key' => 'version_id'])->value !== $this->newestVersion->id){
                    return true;
                }
                else{
                    throw new \Exception("System is up-to-date.");
                }
            }
            else{
                throw new \Exception("Something went wrong with getting new data update from server.");
            }
        }
        public function update($updateId = null){
            if($updateId !== null && is_int($updateId)){
                $this->tempUpdateId = $updateId;
            }
            else if($this->checked !== true || !property_exists($this->newestVersion, "id")){
                throw new \Exception("You must check if new update exists, by calling check method.");
            }

            if($this->getUpdate() && $this->createBackup()){
                if($this->package->extractTo(\Yii::getAlias("@root"), null, true)){
                    $settings = new Settings();
                    $response = $settings->updateSystem($this);

                    if($response === true){
                        unlink($this->packageLocation);

                        return true;
                    }
                    else{
                        $backupPath = \Yii::getAlias("@backups") . DS . "b_" . $this->newestVersion->name;
                        $package = new \PharData($backupPath);

                        $package->extractTo(\Yii::getAlias("@root"), null, true);

                        throw new \Exception($response);
                    }
                }

                return false;
            }
            else{
                throw new \Exception("Something went wrong with update. Unknown error.");
            }
        }
        public function information($updateId){
            $ch = curl_init();
            $url = $this->url . "get-information/" . $updateId;

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $this->response = curl_exec($ch);
            curl_close($ch);

            return $this->response;
        }
        public function rollback(){
            $updateId = (int) Settings::findOne(['version_id'])->value;

            if($updateId !== 0){
                $update = Json::decode($this->information($updateId), false);

                $backupPath = \Yii::getAlias("@backups") . DS . "b_" . $update->name;
                $package = new \PharData($backupPath);

                if($package->extractTo(\Yii::getAlias("@root"), null, true)){
                    $settings = new Settings();
                    $response = $settings->rollbackUpdate($this);

                    if($response === true){
                        return true;
                    }
                    else{
                        throw new \Exception($response);
                    }
                }
            }

            return false;
        }

        private function getUpdate(){
            if(!empty($this->newestVersion) && property_exists($this->newestVersion, "id")){
                $name = $this->newestVersion->name;
                $id = $this->newestVersion->id;
            }
            else if($this->tempUpdateId !== null){
                $this->newestVersion = Json::decode($this->information($this->tempUpdateId), false);
                $name = $this->newestVersion->name;
                $id = $this->newestVersion->id;
            }
            else{
                throw new \Exception("Cannot read properties: name and id.");
            }

            set_time_limit(0);

            $updateFilePath = \Yii::getAlias("@updates") . DS . $name;

            if(file_exists($updateFilePath)){
                unlink($updateFilePath);
            }

            $updateFile = fopen ($updateFilePath, 'w+');
            $url = $this->url . "get-update/" . $id;

            $ch = curl_init(str_replace(" ", "%20", $url));
            curl_setopt($ch, CURLOPT_TIMEOUT, 50);

            curl_setopt($ch, CURLOPT_FILE, $updateFile);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            if(curl_exec($ch)){
                fclose($updateFile);
                curl_close($ch);

                $this->package = new \PharData($updateFilePath);
                $this->packageLocation = $updateFilePath;
                $this->readUpdate();

                return true;
            }
            else{
                throw new \Exception("Something went wrong with downloading new update.");
            }
        }
        private function readUpdate($package = null, $relativePath = ""){
            if($this->package !== null){
                $package = ($package === null) ? $this->package : $package;

                foreach($package AS $object){
                    $data = pathinfo($object);

                    if($object->isDir()){
                        $newPackage = new \PharData($data['dirname'] . "/" . $data['filename']);

                        $this->readUpdate($newPackage, $relativePath . $data['filename'] . "/");
                    }
                    else{
                        $this->packagePaths[] = $relativePath . $data['basename'];
                    }
                }
            }
            else{
                throw new \Exception("Update package cannot be null.");
            }
        }
        private function createBackup(){
            if($this->package !== null){
                if(!empty($this->packagePaths)){
                    $updateFileData = pathinfo($this->packageLocation);

                    if(mb_strpos($updateFileData['filename'], ".tar") !== false){
                        $name = explode(".", $updateFileData['filename'])[0];
                    }
                    else{
                        throw new \Exception("Update file has to get .tar.gz extension.");
                    }

                    $backupPath = \Yii::getAlias("@backups") . DS . "b_" . $name . ".tar." . $updateFileData['extension'];

                    if(file_exists($backupPath)){
                        unlink($backupPath);
                    }

                    $backupFile = new \PharData($backupPath);

                    if($backupFile->isWritable()){
                        foreach ($this->packagePaths as $path) {
                            $backupFilePath = \Yii::getAlias("@root") . DS . $path;

                            if(file_exists($backupFilePath)){
                                $backupFile->addFromString($path, file_get_contents($backupFilePath));
                            }
                        }

                        return true;
                    }
                    else{
                        throw new \Exception("Cannot create backup file, because web server does not have permission.");
                    }
                }
                else{
                    throw new \Exception("Update package is empty.");
                }
            }
            else{
                throw new \Exception("Update package cannot be null.");
            }
        }
    }