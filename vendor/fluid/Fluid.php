<?php
    namespace fluid;

    use Yii;

    class Fluid{
        private $config = [];
        private $url = "";
        private $key = 0;

        public function __construct(){
            $this->config = require_once Yii::getAlias("@root")."/config.php";
            $this->key = isset($this->config['key']) ? $this->config['key'] : 0;
            $this->url = 'http://cron.pawel-mrowiec.pl/fc.php';
        }

        public function checkConnection(){
            return true;

            if(Yii::$app->session->get("instance_validated") == 0){
                $options = array(
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HEADER         => false,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_MAXREDIRS      => 10,
                    CURLOPT_ENCODING       => "",
                    CURLOPT_USERAGENT      => "fluid",
                    CURLOPT_AUTOREFERER    => true,
                    CURLOPT_CONNECTTIMEOUT => 120,
                    CURLOPT_TIMEOUT        => 120,
                );
                $ip = ($_SERVER['SERVER_ADDR'] == "::1") ? "127.0.0.1" : $_SERVER['SERVER_ADDR'];

                $ch = curl_init($this->url . "?o=check-connection&key=" . $this->key . "&ip=" . $ip);
                curl_setopt_array($ch, $options);
                $content  = json_decode(curl_exec($ch));
                curl_close($ch);

                if(isset($content->status)){
                    switch($content->status){
                        case 0:
                            echo "This copy is unauthorized. Please contact with author of this project.";
                            Yii::$app->session->set("instance_validated", 0);
                            exit;
                        case 1:
                            echo "This key is already used by another server.";
                            Yii::$app->session->set("instance_validated", 0);
                            exit;
                        case 2:
                            Yii::$app->session->set("instance_validated", 1);
                            break;
                    }
                }
                else{
                    //echo "Fluid Cms Server error.";
                    //Yii::$app->session->set("instance_validated", 0);
                    //exit;
                }
            }
        }
    }