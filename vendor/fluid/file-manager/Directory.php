<?php
    namespace fluid\fileManager;

    class Directory extends Base
    {
        public $objects;

        public function create($override = false)
        {
            $this->checkExistence();
            $this->handleExistence($override);

            if(mkdir($this->path, 0755, true) === true){
                    $this->checkVisibility();
                    $this->exists = true;

                    return $this;
                }
        }
        public function remove($path = null)
        {
            $this->onCall();

            $path = ($path === null) ? $this->path : $path;
            $objects = array_diff(scandir($path), ['.', '..']);

            foreach ($objects as $object) {
                if(is_dir($path . DS . $object)){
                    $this->remove($path . DS . $object);
                }
                else{
                    unlink($path . DS . $object);
                }
            }

            rmdir($path);

            if($path === $this->path){
                $this->exists = false;
            }

            return $this;
        }
        public function copy($newPath, $override = false, $path = null){
            $this->onCall();

            $this->copyOrMove($newPath, "copy", $override, $path);

            return $this;
        }
        public function move($newPath, $override = false, $path = null){
            $this->onCall();

            $this->copyOrMove($newPath, "move", $override, $path);

            return $this;
        }

        public static function is($path, $newInstance = false){
            parent::handleDirectorySeparator();
            $path = parent::handlePath($path);

            $result = (file_exists($path) && is_dir($path));

            if($newInstance === true){
                if($result === true){
                    return new Directory($path);
                }
                else{
                    return $result;
                }
            }
            else{
                return $result;
            }
        }

        private function copyOrMove($newPath, $operation, $override, $path){
            $directory = new Directory($newPath);
            $newPath = $directory->path;
            $path = ($path === null) ? $this->path : $path;

            if($directory->exists === true){
                if($override){
                    $this->copyOrMoveLogic($newPath, $operation, $override, $path);
                }
                else{
                    throw new \Exception("Object with this name already exists.");
                }
            }
            else{
                $directory->create();
                $this->copyOrMoveLogic($directory->path, $operation, $override, $path);
            }
        }
        private function copyOrMoveLogic($newPath, $operation, $override, $path){
            switch($operation){
                case "copy":
                    $objects = array_diff(scandir($path), ['.', '..']);

                    foreach($objects AS $object){
                        if(is_dir($path . DS . $object)){
                            $tempDirectory = new Directory($newPath . DS . $object);
                            $tempDirectory->create();

                            $this->copy($newPath . DS . $object, $override, $path . DS . $object);
                        }
                        else{
                            copy($path . DS . $object, $newPath . DS . $object);
                        }
                    }
                    break;
                case "move":
                    rename($path, $newPath);
                    break;
            }

        }
    }