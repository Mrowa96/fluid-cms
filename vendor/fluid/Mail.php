<?php
    namespace fluid;

    use Yii;
    use yii\base\Exception;

    class Mail{

        private $connection = null;
        private $ref = null;
        private $server = null;
        private $port = null;
        private $protocol = null;
        private $user = null;

        public function __construct($server, $port, $protocol, $user, $password){
            $this->server = $server;
            $this->port = $port;
            $this->protocol = $protocol;
            $this->user = $user;

            $this->ref = "{";
            $this->ref .= "$server";
            $this->ref .= ($port) ? ":$port" : null;
            $this->ref .= ($protocol) ? "/$protocol" : null;
            $this->ref .= "}";

            $this->connection = imap_open($this->ref, $user, $password);
        }
        public function listMailboxes(){
            print_r(imap_list($this->connection, $this->ref, "*"));die();
        }
    }