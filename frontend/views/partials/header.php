<header id="header" class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <span class="logo">F</span>
            <nav id="mainNav">
                <?php if(isset($this->params['pages'])): ?>
                    <?php foreach($this->params['pages'] AS $page) :?>
                        <a href="#" class="<?php echo (isset($page->activeLink) && $page->activeLink) ? "active" : "";?>" data-nav-static="<?= $page->staticLink;?>" data-nav-url="<?= $page->url; ?>"><?= $page->title; ?></a>
                    <?php endforeach; ?>
                <?php endif; ?>
            </nav>
        </div>
    </div>
</header>