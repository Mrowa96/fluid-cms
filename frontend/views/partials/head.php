<?php
    use yii\helpers\Html;
?>

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="keywords" content="Fluid, CMS">
    <meta name="description" content="Fluid CMS by Paweł Mrowiec">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href='' rel='stylesheet' type='text/css'>
    <?php $this->head() ?>
</head>