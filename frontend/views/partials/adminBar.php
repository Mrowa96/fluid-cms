<?php if(isset($this->params['isAdmin']) && $this->params['isAdmin'] === true): ?>
    <div id="adminBar" class="container-fluid">
        <a href="<?= Yii::getAlias("@backendLink"); ?>" class="dashBtn">
            <i class="fa fa-dashboard"></i>
        </a>
        <?php if(isset($this->params['page']) && $this->params['page']->id): ?>
            <?php
                if((!isset($this->params['lockEdit']) || $this->params['lockEdit'] !== true)){
                    $style = "style='opacity: 1; display: block;'";
                }
                else{
                    $style = "style='opacity: 0; display: none;'";
                }
            ?>
            <a <?= $style; ?> data-base-url="<?= Yii::getAlias("@backendLink")."/page/update.html?id="; ?>" href="<?= Yii::getAlias("@backendLink")."/page/update.html?id=".$this->params['page']->id ?>" class="editBtn">
                <i class="fa fa-pencil"></i>
            </a>
        <?php endif; ?>
        <a class="info" href="<?= Yii::getAlias("@backendLink")."/user/profile/".Yii::$app->user->identity->id; ?>.html">
            <img src="<?= Yii::$app->user->identity->avatar; ?>" class="img-responsive" alt="User">
            <span>
                <?= Yii::$app->user->identity->username; ?>
            </span>
        </a>
    </div>
<?php endif; ?>
