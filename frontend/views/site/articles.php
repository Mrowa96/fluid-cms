<?php
use yii\helpers\Url;
?>
<div class="articles-page">
    <div class="container">
        <div class="row">
            <?php if(!empty($data)): ?>
                <?php foreach($data AS $index => $article): ?>
                    <?php if($index % 2 === 0): ?>
                        <div class="row">
                    <?php endif; ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="article">
                            <div class="image">
                                <a href="<?= Url::toRoute(["site/article", 'title' => $article->title, 'id' => $article->id, 'pageId' => $this->params['page']->id]); ?>">
                                    <img src="<?= $article->image; ?>" class="img-responsive">
                                </a>
                            </div>
                            <div class="title">
                                <a href="<?= Url::toRoute(["site/article", 'title' => $article->title, 'id' => $article->id, 'pageId' => $this->params['page']->id]); ?>">
                            </div>
                            <div class="lead">
                                <span><?= $article->lead; ?></span>
                            </div>
                        </div>
                    </div>
                    <?php if($index % 2 === 1): ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 emptyArticles">
                    <i class="fa fa-keyboard-o"></i>
                    <span>W trakcie pisania...</span>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
