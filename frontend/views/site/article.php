<div class="article-page">
    <div class="container">
        <div class="row">
            <div class="article">
                <div class="image col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="<?= $article->image; ?>" class="img-responsive">
                </div>
                <div class="title col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <?= $article->title; ?>
                </div>
                <div class="text col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <span><?= $article->text; ?></span>
                </div>
                <div class="informations col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="created">
                        <?= $article->created; ?>
                    </div>
                    <div class="author">
                        <?= $article->user->username; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
