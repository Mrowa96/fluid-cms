<?php
    use common\widgets\Alert;
    use frontend\assets\AppAsset;

    AppAsset::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>

    <html lang="<?= Yii::$app->language ?>">
        <?= $this->render("@frontend/views/partials/head"); ?>

        <body>
        <?php $this->beginBody() ?>
            <div class="wrapper">
                <?= $this->render("@frontend/views/partials/adminBar"); ?>
                <?= $this->render("@frontend/views/partials/header"); ?>
                <?= Alert::widget() ?>

                <div id="content">
                    <?php
                        if(isset($content)){
                            echo $content;
                        }
                    ?>
                </div>
                <?= $this->render("@common/views/partials/loadScreen"); ?>
                <?= $this->render("@common/views/partials/fluidCmsVer"); ?>
            </div>
        <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>
