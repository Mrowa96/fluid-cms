<?php

    namespace frontend\controllers;

    use backend\models\Group;
    use Yii;
    use yii\filters\AccessControl;
    use yii\web\Controller;

    class BaseController extends Controller{

        protected $isAdmin = false;

        public function init(){
            $this->view->title = "Fluid CMS - Example";

            Yii::$app->fluid->checkConnection();

            $user = Yii::$app->user->identity;

            if($user){
                $roles = Yii::$app->authManager->getRolesByUser($user->id);

                if(isset($roles['admin'])){
                    $this->isAdmin = true;
                    $this->view->params['isAdmin'] = $this->isAdmin;
                }
            }

            parent::init();
        }

        public function actions(){
            return [
                'error' => [
                    'class' => 'common\controllers\ErrorController',
                ],
                'captcha' => [
                    'class' => 'yii\captcha\CaptchaAction',
                    'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                ],
            ];
        }
    }