<?php
    namespace frontend\controllers;

    use common\models\Article;
    use Yii;
    use yii\helpers\Json;
    use yii\web\HttpException;
    use yii\web\NotFoundHttpException;
    use fluid\Lib;
    use backend\models\Template;
    use common\models\Module;
    use common\models\Page;
    use common\models\Visit;
    use frontend\models\ContactForm;

    class SiteController extends BaseController{

        private $url = null;
        private $urlParts = [];
        private $activeModule = null;
        private $activePage= null;
        private $activeTemplate= null;
        private $isIndex = null;
        private $getContent = false;
        private $content = null;
        private $script = [];
        private $style = [];
        private $styleFiles = [];
        private $scriptFiles = [];
        private $templateId = null;
        private $reloadScript = false;
        private $lockEdit = false;

        public function beforeAction($action){
            $data = Json::decode(Yii::$app->request->post("data"));

            if(Yii::$app->request->isPost && !empty($data)){
                $this->enableCsrfValidation = false;

                if($data['command'] === "getContent"){
                    $this->getContent = true;
                    $this->layout = false;
                }
            }

            return parent::beforeAction($action);
        }
        public function afterAction($action, $result){
            if($this->content !== null){
                $response = [
                    'status' => 1,
                    'data' => $this->content,
                    'script' => $this->script,
                    'style' => $this->style,
                    'styleFiles' => $this->styleFiles,
                    'scriptFiles' => $this->scriptFiles,
                    'reloadScript' => $this->reloadScript,
                    'lockEdit' => $this->lockEdit,
                    'meta' => [
                        'keywords' => $this->activePage->keywords,
                        'description' => $this->activePage->description,
                        'title' => $this->activePage->title,
                    ],
                    'id' => $this->activePage->id,

                ];

                return Json::encode($response);
            }

            return parent::afterAction($action, $result);
        }

        public function actionIndex(){
            $this->handleUrl();
            $this->handleSpecyfic();

            $visit = new Visit();
            $visit->date = date("Y-m-d H:i:sa");
            $visit->ip = Yii::$app->request->userIP;
            $visit->save();

            if(!$this->activeTemplate->php_action) {
                if($this->getContent){
                    $this->layout = false;
                    $this->content = $this->activePage->data;
                }
                else{
                    $this->view->title = $this->activePage->title;
                    $this->view->params['module'] = $this->activeModule;
                    $this->view->params['page'] = $this->activePage;
                    $this->view->params['pages'] = Page::getPagesForNavigation($this->activeModule->id);

                    return $this->renderContent($this->activePage->data);
                }
            }
            else {
                $action = Lib::concatOnUpper($this->activeTemplate->php_action, "-");
                $action = ucfirst($action);
                $action = "action".$action;

                if($this->getContent){
                    $this->layout = false;
                    $this->view->params['page'] = $this->activePage;
                    $this->content = $this->renderContent($this->{$action}());

                    $this->handleDynamicAssets();
                }
                else{
                    $this->view->title = $this->activePage->title;
                    $this->view->params['module'] = $this->activeModule;
                    $this->view->params['page'] = $this->activePage;
                    $this->view->params['pages'] = Page::getPagesForNavigation($this->activeModule->id);

                    return $this->{$action}();
                }
            }
        }
        public function actionContact(){
            $model = new ContactForm();

            if ($model->load(Yii::$app->request->post()) && $model->sendEmail(Yii::$app->params['adminEmail'])){
                Yii::$app->session->setFlash('contactFormSubmitted');

                return $this->refresh();
            }
            else{
                return $this->render("contact", [
                    'model' => $model
                ]);
            }
        }
        public function actionArticles(){
            $data = Article::find()->all();

            return $this->render("articles", [
                'data' => $data
            ]);

        }
        public function actionArticle($title, $id, $pageId){
            $article = Article::findOne($id);
            $page = Page::findOne($pageId);
            $module = Module::findOne($page->module_id);

            $this->lockEdit = true;

            $this->view->params['pages'] = Page::getPagesForNavigation($module->id, $page->id);
            $this->view->params['page'] = $page;
            $this->view->params['lockEdit'] = $this->lockEdit;

            if($article){
                return $this->render("article", [
                    'article' => $article
                ]);
            }
            else{
                throw new HttpException(404, 'The requested Article could not be found.');
            }
        }

        private function handleUrl(){
            $url = Yii::$app->request->getUrl();

            if(strpos($url, "?") !== false){
                $url = substr($url, 0, strpos($url, "?"));
            }

            //first parting
            switch($url){
                case "/index":
                    $url= "/";
                    break;
                default:
                    $tempUrl = explode(".", $url);

                    if(count($tempUrl) === 2){
                        $url = $tempUrl[0];
                    }
                    else{
                        //throw error
                    }
                    break;
            }

            //second parsing
            $parts = explode("/", $url);

            if(isset($parts[0]) && $parts[0] === ""){
                foreach($parts AS $key => $value){
                    if($key < count($parts) - 1){
                        $parts[$key] = $parts[$key + 1];
                    }
                }

                array_pop($parts);
            }

            $this->url = $url;
            $this->urlParts = $parts;
        }
        private function handleSpecyfic(){
            $modules = Module::find()->where(['active' => 1])->all();
            $pages = Page::find()->where(['active' => 1])->all();
            $template = null;

            foreach($modules AS $module){
                if($module->url === $this->url){
                    $this->activeModule = $module;
                    $this->isIndex = true;
                    break;
                }
            }

            if($this->isIndex){
                foreach($pages AS $page){
                    if($page->url === "/" && $page->module_id === $this->activeModule->id){
                        $this->activePage = $page;
                        $this->handleTemplateData(Template::findOne($page->template_id));
                        return;
                    }
                }
            }

            if($this->activeModule === null){
                foreach($this->urlParts AS $part){
                    if($this->activeModule === null){
                        foreach($modules AS $module){
                            if("/".$part === $module->url){
                                $this->activeModule = $module;

                                break;
                            }
                        }
                    }

                    if($this->activePage === null){
                        foreach($pages AS $page){
                            if("/".$part === $page->url){
                                $this->activePage = $page;
                                $this->handleTemplateData(Template::findOne($page->template_id));

                                break;
                            }
                        }
                    }
                }
            }

            if($this->activeModule === null && $this->activePage !== null){
                $this->activeModule = Module::findOne($this->activePage->module_id);
            }

            if($this->activeModule === null || $this->activePage === null){
                throw new NotFoundHttpException("Couldn't find requested action.");
            }
        }
        private function handleTemplateData($template){
            $this->activeTemplate = $template;

            if($this->getContent){
                $this->style[] = $template->css_data;
                $this->script[] = $template->js_data;
                $this->templateId = $template->id;

            }
            else{
                $this->view->registerCss($template->css_data);
                $this->view->registerJs($template->js_data);
            }
        }
        private function handleDynamicAssets(){
            $assets = $this->view->assetManager->bundles;

            if(!empty($assets)){
                foreach($assets AS $asset){
                    if(!empty($asset->js)){
                        foreach($asset->js AS $js){
                            $this->scriptFiles[] = $asset->baseUrl."/".$js;
                        }
                    }

                    if(!empty($asset->css)){
                        foreach($asset->css AS $css){
                            $this->styleFiles[] = $asset->baseUrl."/".$css;
                        }
                    }
                }
            }

            if(!empty($this->view->js)){
                $this->reloadScript = true;

                foreach($this->view->js AS $js){
                    if(is_array($js)){
                        foreach($js AS $data){
                            $this->script[] = $data;
                        }
                    }
                }
            }
        }
    }
