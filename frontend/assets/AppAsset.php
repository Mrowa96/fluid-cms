<?php
    namespace frontend\assets;

    use Yii;
    use yii\web\AssetBundle;

    class AppAsset extends AssetBundle
    {
        public $basePath = '@webroot';
        public $baseUrl = '@web';
        public $css = [
            "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            'https://fonts.googleapis.com/css?family=Poiret+One|Open+Sans:400,300,600&subset=latin,latin-ext',
            'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200&subset=latin,latin-ext',
            CDN."/font-awesome/css/font-awesome.min.css",
            CDN.'/fluid-cms/styles/common/app.css',
            CDN.'/fluid-cms/styles/frontend/app.css',
            CDN.'/fluid-elements/styles/app.css',
        ];
        public $js = [
            CDN.'/fluid-elements/scripts/fluid-core.js',
            CDN.'/fluid-cms/scripts/frontend/_fluid-elements/fluid-instance.js',
        ];
        public $depends = [
            'yii\web\YiiAsset',
        ];
    }
