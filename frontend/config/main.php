<?php
    $params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'),
        require(__DIR__ . '/params.php')
    );

    $config = [
        'id' => 'app-frontend',
        'language' => 'en_US',
        'basePath' => dirname(__DIR__),
        'bootstrap' => ['log'],
        'controllerNamespace' => 'frontend\controllers',
        'components' => [
            'user' => [
                'identityClass' => 'common\models\User',
                'enableAutoLogin' => true,
                'identityCookie' => [
                    'name' => '_identity',
                    'path' => '/',
                    'domain' => Yii::getAlias("@domainLink"),
                ],
            ],
            'log' => [
                'traceLevel' => YII_DEBUG ? 3 : 0,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                'enableStrictParsing' => false,
                'rules' => [
                    '/' => 'site/index',
                    '/article/<title:(.*?)+>,<id:[0-9]+>,<pageId:[0-9]+>' => 'site/article',
                    '/<param1:[A-Za-z0-9\-]+>' => 'site/index',
                    '/<param1:[A-Za-z0-9\-]+>/<param2:[A-Za-z0-9\-]+>' => 'site/index',
                ],
                'suffix' => '.html'
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
        ],
        'params' => $params,
    ];

    if (!YII_ENV_TEST) {
        $config['bootstrap'][] = 'debug';
        $config['modules']['debug'] = 'yii\debug\Module';

        $config['bootstrap'][] = 'gii';
        $config['modules']['gii'] = 'yii\gii\Module';
    }

    return $config;
